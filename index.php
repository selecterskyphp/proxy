<?php
$u = $_GET['u'];
$t = $_GET['t'];
$file = '';
if(!$u)
{
	die('error');
}
if($t)
{
	$arr = explode('/',$u);
	$filename = $arr[count($arr)-1];
}
$protocol = "http";
if(strpos($u,":")>0)
{
	$arr = explode(":",$u);
	$protocol = $arr[0];
}
//$requestUrl = 'ip138.com';
$ch = curl_init();
$timeout = 5;
curl_setopt($ch, CURLOPT_URL, $u);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
$body = curl_exec($ch);
curl_close($ch);

if($t)
{
	down($body,$filename);
	exit();
}
//特殊处理
$body = preg_replace("/src=\"\/\//is","src=\"".$protocol."://",$body);
$body = preg_replace("/src=\'\/\//is","src=\'".$protocol."://",$body);
$body = preg_replace("/href=\"\/\//is","href=\"".$protocol."://",$body);
$body = preg_replace("/href=\'\/\//is","href=\'".$protocol."://",$body);
//先将所有的没有http的链接加http
//href
$body = preg_replace("/href=\"(?!http)/is", "href=\"".$u, $body);
$body = preg_replace("/href=\'(?!http)/is", "href=\'".$u, $body);

//src
$body = preg_replace("/src=\"(?!http)/is", "src=\"".$u, $body);
$body = preg_replace("/src=\'(?!http)/is", "src=\'".$u, $body);

//css
$body = preg_replace("/url\(\'/is","url(",$body);
$body = preg_replace("/url\((?!http)/is", "url(".$u, $body);

//action
$body = preg_replace("/action=\"(?!http)/is","action=\"".$u,$body);
$body = preg_replace("/action=\'(?!http)/is","action='".$u,$body);

//然后加上自定义的链接
//href
$body = preg_replace("/href=\"/is","href=\"?u=",$body);
$body = preg_replace("/href=\'/is","href='?u=",$body);

//img
$body = preg_replace("/src=\"/is","src=\"?u=",$body);
$body = preg_replace("/src=\'/is","src=\'?u=",$body);

//css
$body = preg_replace("/url\(/is","url(?u=",$body);

//action
$body = preg_replace("/action=\"/is","action=\"?u=",$body);
$body = preg_replace("/action=\'/is","action='?u=",$body);

echo $body;


function down($body, $filename)
{
	Header("Content-type: application/octet-stream");
	Header("Accept-Ranges: bytes");
	Header("Accept-Length: ".strlen($body));
	Header("Content-Disposition: attachment; filename=" . $filename);
	echo $body;
}
?>
